package id.ac.tazkia.spmb.aplikasispmb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiSpmbApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiSpmbApplication.class, args);
	}

}
